# multiAgents.py
# --------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
#
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


from util import manhattanDistance,Queue
from game import Directions,Agent
import random, util


#distance between the start point and the point somePoint
class ReflexAgent(Agent):
    """
      A reflex agent chooses an action at each choice point by examining
      its alternatives via a state evaluation function.

      The code below is provided as a guide.  You are welcome to change
      it in any way you see fit, so long as you don't touch our method
      headers.
    """


    def getAction(self, gameState):
        """
        You do not need to change this method, but you're welcome to.

        getAction chooses among the best options according to the evaluation function.

        Just like in the previous project, getAction takes a GameState and returns
        some Directions.X for some X in the set {North, South, West, East, Stop}
        """
        # Collect legal moves and successor states
        legalMoves = gameState.getLegalActions()

        # Choose one of the best actions
        scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
        bestScore = max(scores)
        bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
        chosenIndex = random.choice(bestIndices) # Pick randomly among the best

        "Add more of your code here if you want to"

        return legalMoves[chosenIndex]

    def evaluationFunction(self, currentGameState, action):
        """
        Design a better evaluation function here.

        The evaluation function takes in the current and proposed successor
        GameStates (pacman.py) and returns a number, where higher numbers are better.

        The code below extracts some useful information from the state, like the
        remaining food (newFood) and Pacman position after moving (newPos).
        newScaredTimes holds the number of moves that each ghost will remain
        scared because of Pacman having eaten a power pellet.

        Print out these variables to see what you're getting, then combine them
        to create a masterful evaluation function.
        """
        # Useful information you can extract from a GameState (pacman.py)
        # successorGameState = currentGameState.generatePacmanSuccessor(action)
        # newPos = successorGameState.getPacmanPosition()
        # newFood = successorGameState.getFood()
        # newGhostStates = successorGameState.getGhostStates()
        # newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]

        "*** YOUR CODE HERE ***"
        State = currentGameState.generateSuccessor(0, action)
        currentPosition = State.getPacmanPosition()
        donefood = State.getFood().asList() + State.getCapsules()
        bfsLengthDict = {}
        bfsLengthDict[currentPosition] = bfsDistance(State.getWalls().deepCopy(), currentPosition) # bfs map creation
        bfsDists = bfsLengthDict[currentPosition]
        scoreValue = State.getScore() #getting score

        temp1 = []
        for x in State.getGhostStates():
            temp1.append([x.scaredTimer, x.getPosition()])
        ghostScare=[]
        for j in temp1:
            if (j[0] > 0):
                ghostScare.append(j)

        ghostFoodDistance = map(lambda i: i[1],filter(lambda d: d[1] < d[0],[(s[0], bfsDists[int(s[1][0])][int(s[1][1])]) for s in ghostScare]))
        if 0 not in (ghostFoodDistance) and len(ghostFoodDistance) > 0: #ghost score modifying
            scoreValue += 10.0 / min(ghostFoodDistance)

        foodDist = []
        for food in donefood:
            foodDist.append(bfsDists[food[0]][food[1]])



        if len(foodDist) > 0:
            scoreValue += 1.0 / min(foodDist)

        return scoreValue

def scoreEvaluationFunction(currentGameState):
    """
      This default evaluation function just returns the score of the state.
      The score is the same one displayed in the Pacman GUI.

      This evaluation function is meant for use with adversarial search agents
      (not reflex agents).
    """
    return currentGameState.getScore()

class MultiAgentSearchAgent(Agent):
    """
      This class provides some common elements to all of your
      multi-agent searchers.  Any methods defined here will be available
      to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

      You *do not* need to make any changes here, but you can if you want to
      add functionality to all your adversarial search agents.  Please do not
      remove anything, however.

      Note: this is an abstract class: one that should not be instantiated.  It's
      only partially specified, and designed to be extended.  Agent (game.py)
      is another abstract class.
    """

    def __init__(self, evalFn = 'scoreEvaluationFunction', depth = '2'):
        self.index = 0 # Pacman is always agent index 0
        self.evaluationFunction = util.lookup(evalFn, globals())
        self.depth = int(depth)

class MinimaxAgent(MultiAgentSearchAgent):
    """
      Your minimax agent (question 2)
    """


    def miniMax(self, State, depth, aIndex=0):

        if State.isWin() or State.isLose() or depth == 0:
            return (self.evaluationFunction(State),) #return current score
        Agent = State.getNumAgents()
        if aIndex != Agent - 1:
            nDepth = depth
        else:
            nDepth = depth - 1
        newAIndex = (aIndex + 1) % Agent
        actionLegal=State.getLegalActions(aIndex)
        temp5 = []
        for a in actionLegal:
            temp5.append((self.miniMax(State.generateSuccessor(aIndex, a), nDepth, newAIndex)[0], a))
        z=max(temp5) if aIndex == 0 else min(temp5)
        return z

    def getAction(self, gameState):
        """
          Returns the minimax action from the current gameState using self.depth
          and self.evaluationFunction.

          Here are some method calls that might be useful when implementing minimax.

          gameState.getLegalActions(agentIndex):
            Returns a list of legal actions for an agent
            agentIndex=0 means Pacman, ghosts are >= 1

          gameState.generateSuccessor(agentIndex, action):
            Returns the successor game state after an agent takes an action

          gameState.getNumAgents():
            Returns the total number of agents in the game
        """
        "*** YOUR CODE HERE ***"
        return self.miniMax(gameState, self.depth)[1]




class AlphaBetaAgent(MultiAgentSearchAgent):
    """
      Your minimax agent with alpha-beta pruning (question 3)
    """

    def maxvalue(self, state, aIndex, currentdepth, a, b):
        v = (float("-inf"), "Stop")
        m=currentdepth + 1
        n=m% self.number_of_agents
        for action in state.getLegalActions(aIndex):
            v = max([v, (self.value(state.generateSuccessor(aIndex, action),n,m, a, b), action)], key=lambda i: i[0])
            if v[0] > b:
             return v
            a= max(a, v[0])
        return v

    def minvalue(self, state, aIndex, currentdepth, a, b):
        v = (float("inf"), "Stop")
        m=currentdepth + 1
        n=m % self.number_of_agents
        temp=state.getLegalActions(aIndex)
        for action in temp:
            v = min([v, (self.value(state.generateSuccessor(aIndex, action),n,m, a, b), action)], key=lambda i: i[0])
            if v[0] < a:
                    return v
            b= min(b, v[0])
        return v


    def value(self, state, aIndex, currentdepth, a, b):
        if state.isLose() or state.isWin() or currentdepth >= self.depth*self.number_of_agents:
            return self.evaluationFunction(state)
        retVal= (self.maxvalue(state, aIndex, currentdepth, a, b)[0]) if (aIndex == 0) else (self.minvalue(state, aIndex, currentdepth, a, b)[0])
        return retVal


    def getAction(self, gameState):
        "*** YOUR CODE HERE ***"
        self.number_of_agents = gameState.getNumAgents()
        a = float("-inf")
        b = float("inf")
        return self.maxvalue(gameState, 0, 0,a,b)[1]


class ExpectimaxAgent(MultiAgentSearchAgent):
    """
      Your expectimax agent (question 4)
    """
    def expectiMax(self, State, depth, aIndex=0):
        if State.isWin() or State.isLose() or depth == 0:
            return (self.evaluationFunction(State),)
        Agents = State.getNumAgents()
        if aIndex != Agents - 1:
            nDepth = depth
        else:
            nDepth=depth - 1
        newAIndex = (aIndex + 1) % Agents
        actionLegal = State.getLegalActions(aIndex)
        temp=[]
        for a in actionLegal:
            temp.append ( (self.expectiMax(State.generateSuccessor(aIndex, a),nDepth, newAIndex)[0], a))

        retVal=max(temp) if (aIndex == 0) else (reduce(lambda x, y: x + y[0], temp, 0) / len(actionLegal),)
        return retVal

    def getAction(self, gameState):
        """
          Returns the expectimax action using self.depth and self.evaluationFunction

          All ghosts should be modeled as choosing uniformly at random from their
          legal moves.
        """
        "*** YOUR CODE HERE ***"
        return self.expectiMax(gameState, self.depth)[1]

def betterEvaluationFunction(currentGameState):
    """
      Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
      evaluation function (question 5).

      DESCRIPTION: <write something here so we know what you did>
    """
    "*** YOUR CODE HERE ***"
    # State = currentGameState.generateSuccessor(0, action)
    currentPosition = currentGameState.getPacmanPosition()
    donefood = currentGameState.getFood().asList() + currentGameState.getCapsules()
    bfsLengthDict = {}
    bfsLengthDict[currentPosition] = bfsDistance(currentGameState.getWalls().deepCopy(), currentPosition)  # bfs map creation
    bfsDists = bfsLengthDict[currentPosition]
    scoreValue = currentGameState.getScore()  # getting score

    #: list of (timer, position) of all scared ghosts
    temp1 = []
    for x in currentGameState.getGhostStates():
        temp1.append([x.scaredTimer, x.getPosition()])
    ghostScare = []
    for j in temp1:
        if (j[0] > 0):
            ghostScare.append(j)

    temp2=[]
    temp3=[]
    for s in ghostScare:
        temp2.append((s[0], bfsDists[int(s[1][0])][int(s[1][1])]))
    for k in temp2:
        if(k[1] < k[0]):
            temp3.append(k)
    ghostFoodDistance = map(lambda i: i[1],temp3)
    if 0 not in (ghostFoodDistance) and len(ghostFoodDistance) > 0:  # ghost score modifying
        scoreValue += 10.0 / min(ghostFoodDistance)

    foodDist=[]
    for food in donefood:
        foodDist.append(bfsDists[food[0]][food[1]])

    if len(foodDist) > 0:
        scoreValue += 1.0 / min(foodDist)

    return scoreValue


def bfsDistance(somePoint, start):
  Fringe = Queue()
  Fringe.push(start)
  somePoint[ start[0] ][ start[1] ] = 0
  coordinate = [(1,0), (0,1), (-1,0), (0,-1)]
  while not Fringe.isEmpty():
    currentState = Fringe.pop()
    Distance = somePoint[ currentState[0] ][ currentState[1] ]

    for coord in coordinate:
      position = (currentState[0] + coord[0], currentState[1] + coord[1])
      if position[0]>=0 and position[0]<somePoint.width and position[1]>=0 and position[1]<somePoint.height and \
         type(somePoint[ position[0] ][ position[1] ]).__name__ == 'bool' and somePoint[ position[0] ][ position[1] ] == False:
          somePoint[position[0]][position[1]] = Distance + 1 #distance added to map
          Fringe.push(position)
  return somePoint

# Abbreviation
better = betterEvaluationFunction

